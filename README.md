## setup aws credential on local
```bash
$ aws configure
```

## installation of serverless-cli
```bash
$ npm install -g serverless
```

## installation of node modules/packages
```bash
$ npm install
```

## start application locally
```bash
$ serverless offline start
```

## deploy on server
```bash
$ serverless deploy
```

---
**NOTE**

- Need to set USER_POOL_ID in environment in serverless file, You can get value from aws console > cognito > user pool > service-employee-pool-dev > general settings > pool id

- Need to signup and signin in aws console > cognito > user pool > service-employee-pool-dev > App Integration > App client settings > Hosted UI

- Once login it will redirect to localhost:3000... and need to take id_token ONLY from url and use it in postman > header > Authorization with Bearer 

- aws configure > it will need to have permission of S3, cognito, cloudformation, SQS, DynamoDB, lambda, cloudwatch etc

- postman collection : https://www.getpostman.com/collections/f0ae45689560c48a0da8
---