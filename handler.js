const AWS = require("aws-sdk");
const express = require("express");
const serverless = require("serverless-http");
const uuid = require('uuid');

const app = express();

// Create SQS service client
const sqs = new AWS.SQS({apiVersion: '2012-11-05'});

// Replace with your account id and the queue name you setup
const accountId = process.env.ACCOUNT;
const queueName = process.env.QUEUE;

const EMPLOYEE_TABLE = process.env.EMPLOYEE_TABLE;
const dynamoDbClient = new AWS.DynamoDB.DocumentClient();

app.use(express.json());

app.get("/employees", async function (req, res) {
  const params = {
    TableName: EMPLOYEE_TABLE
  };

  try {
    const { Item } = await dynamoDbClient.scan(params).promise();

    let scanResults = [];
    let items;
    do{
        items =  await dynamoDbClient.scan(params).promise();
        items.Items.forEach((item) => scanResults.push(item));
        params.ExclusiveStartKey  = items.LastEvaluatedKey;
    }while(typeof items.LastEvaluatedKey != "undefined");

    res.json(scanResults);
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Could not retreive users" });
  }
});

app.get("/employees/:id", async function (req, res) {
  const params = {
    TableName: EMPLOYEE_TABLE,
    Key: {
      id: req.params.id,
    },
  };

  try {
    const { Item } = await dynamoDbClient.get(params).promise();
    if (Item) {
      const { id, name } = Item;
      res.json({ id, name });
    } else {
      res
        .status(404)
        .json({ error: 'Could not find user with provided "id"' });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Could not retreive user" });
  }
});

app.post("/employees", async function (req, res) {
  const { name } = req.body;
  if (typeof name !== "string") {
    res.status(400).json({ error: '"name" must be a string' });
  }

  let id = uuid.v1();

  const params = {
    TableName: EMPLOYEE_TABLE,
    Item: {
      id: id,
      name: name,
    },
  };

  try {
    await dynamoDbClient.put(params).promise();

    // Setup the sendMessage parameter object
    const params_sql = {
      MessageBody: JSON.stringify({
        id: id,
        name: name,
        date: (new Date()).toISOString()
      }),
      QueueUrl: `https://sqs.us-east-1.amazonaws.com/${accountId}/${queueName}`
    };
    sqs.sendMessage(params_sql, (err, data) => {
      if (err) {
        console.log("Error", err);
      } else {
        console.log("Successfully added message", data.MessageId);
      }
    });
    res.json({ id, name });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Could not create user" });
  }
});

app.use((req, res, next) => {
  return res.status(404).json({
    error: "Not Found",
  });
});


module.exports.handler = serverless(app);